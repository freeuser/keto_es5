
    var url = window.cdn_path;
    if (url === undefined) {
      url = "";
    }
    const MSG_DELAY = 1, TYPE_SPEED = 1;
    let flow = [{
        type: "text",
        content: "Hola, mi nombre es Andrea Gonzalez. Soy dietologa, nutricionista, especialista en medicina funcional e integrativa. Bienvenido a mi sitio web oficial."
    }, {
        type: "text",
        content: "Aquí proporciono diagnósticos en línea gratuitos y recomendaciones personales que han ayudado a cientos de mujeres y hombres a perder peso."
    }, {
        type: "text",
        content: "Por favor responda algunas preguntas para recibir mis consejos de pérdida de peso."
    }, {

        type: "single_choice",
        content: "¿Cuántos kilogramos quiere perder?",
        answers: ["Hasta 5 kg", "De 5 a 10 kg", "De 10 a 15 kg", "Más de 15 kg"]
    }, {
        type: "single_choice",
        content: "¿Qué partes de su cuerpo requieren corrección?",
        answers: ["Cintura y vientre", "Caderas y glúteos", "Brazos y piernas", "Quiero adelgazar en todas partes"]
    }, {
        type: "single_choice",
        content: "Quiero adelgazar en todas partes",
        answers: ["Entreno 3-5 veces a la semana", "Entreno 1-3 veces a la semana", "No hago ejercicio, pero trato de caminar", " Estilo de vida sedentario"]
    }, {
        type: "single_choice",
        content: "¿Se alimenta bien?",
        answers: ["Sí, cuento calorías, no como dulces ni harina", "Entre un 50/50", "Me encantan los dulces, la comida rápida y lo \"chatarra\"", "Como con lo que me alcanza"]
    }, {

        type: "single_choice",
        content: "¿Su edad?",
        answers: ["Hasta 20 años", "De 20-30 años", "De 31-40 años", "De 41-50 años", "Más de 51 años"]
    }, {
        type: "single_choice",
        content: "¿Cuál es su peso actual?",
        answers: ["Hasta 60 kg", "De 60 a 70 kg", "De 71 a 80 kg", "Más de 80 kg"]
    }, {
        type: "single_choice",
        content: "¿Su altura?",
        answers: ["Por debajo de 150 cm", "150-160 cm", "161-170 cm", "171-180 cm", "Por encima de 180 cm"]
    }, {
        type: "text",
        content: "Gracias. Tiene exceso de peso, pero es muy posible que logre su objetivo deseado de perder peso."
    }, {
        type: "text",
        content: "Ahora, para ponerse en forma, no es necesario torturarse con dietas y entrenamientos. La primera recomendación, que siempre funciona, es tomar al menos 2 litros de agua al día. El agua ayudará a normalizar la digestión, limpiará el cuerpo de toxinas y residuos."
    }, {
        type: "text",
        content: "Para mejorar el efecto, tome más té verde y agregue espirulina a su dieta. Las hojas de té contienen catequinas, que activan el metabolismo del cuerpo y eliminan toxinas y sales de metales pesados. La espirulina ayuda a reducir los niveles de lípidos y colesterol."
    }, {
        type: "text",
        content: "Para iniciar el proceso de quema de grasa, se requiere L-carnitina que proporciona ácidos grasos a las mitocondrias y ayuda a procesarlos en energía."
    }, {
        type: "text",
        content: "Los componentes enumerados se pueden usar por separado, pero son mucho más convenientes y efectivos como parte del complemento alimenticio <span style='display: inline-block;'>Keto Eat and Fit</span>. Tanto la espirulina como el té verde están presentes en forma de extractos intensos con una concentración máxima de nutrientes."
    },
        {
        type: "text",
        content: "Para una disminución notable del peso corporal, basta con echar la cápsula en un vaso de agua 20 minutos antes de una comida y tomarla una vez por la mañana, y en un mes podrás alcanzar tu objetivo."
    },
        {
        type: "text",
        content: "Así es como se ve: <br><br> <img src='"+ url +"images/tov.png' style='width:50%;max-width:233px'> "
    }, {
        type: "text",
        content: "Con la descomposición intensiva de la grasa bajo la acción de las cápsulas <span style='display: inline-block;'>Keto Eat and Fit</span>, se libera energía útil, que le da al un poderoso impulso al adelgazamiento, elimina el estado depresivo, alivia la apatía y el cansancio y aumenta su rendimiento. ¡Este innovador complejo aún no tiene análogos!"
    }, {
        type: "text",
        content: "<span style='display: inline-block;'>Keto Eat and Fit</span> descompone y elimina de manera muy efectiva la grasa visceral, que envuelve las paredes de los órganos internos y evita que funcionen normalmente, desbloquea el metabolismo limpiando el cuerpo de toxinas y residuos."
    }, {
        type: "text",
        content: "Como resultado, el organismo comienza a deshacerse del exceso de grasa por sí mismo, usándolo como fuente de energía. ¡Come menos al no tener hambre, lo que mejora aún más el efecto adelgazante!"
    }, {
        type: "text",
        content: "Estos son los resultados de mis clientes que han perdido peso con éxito siguiendo mis recomendaciones: <br> <img src='"+ url +"images/c1.jpg'> <br> <img src='"+ url +"images/c2.jpg'> <br> <img src='"+ url +"images/c3.jpg'>"
    }, {
        type: "text",
        content: "La duración óptima del curso, que tiene en cuenta su edad, índice de masa corporal actual y estilo de vida, es de 30 días."
    }, {
        type: "text",
        content: "Durante este tiempo, se producirá una limpieza profunda del cuerpo y la normalización del metabolismo."
    }, {
        type: "text",
        content: "También tengo una gran noticia para ti. ¡Aprobó los diagnósticos en línea y se convirtió en mi cliente número 2000!"
    }, {
        type: "text",
        content: "Solo hoy tienes la oportunidad de obtener <span style='display: inline-block;'>Keto Eat and Fit</span> con un 50% de descuento como parte de la promoción del fabricante."
    }, {
        type: "text",
        content: "Para obtener <span style='display: inline-block;'>Keto Eat and Fit</span>, escriba su nombre y número de teléfono en el formulario de pedido a continuación. Sus datos se envían directamente al fabricante, nadie más tiene acceso a ellos."
    }, {
        type: "text",
        content: "Un especialista le devolverá la llamada y tras aclarar todos los detalles, el mismo día se enviará un paquete con el curso <span style='display: inline-block;'>Keto Eat and Fit</span>."
    }, {
        type: "text",
        content: "El número de paquetes para la promoción es limitado, por lo que recomiendo apresurarse con el pedido."
    }, {type: "form", templateId: "orderForm"}];

    function createSingleChoiceForm(t) {
        let e = t.reduce((t, e) => t + `<button type="button" class="answer" data-answer>${e}</button>`, "");
        return $(`<div class="answers">${e}</div>`)
    }

    $(document).ready(function () {
      flow.reduce((t, e) => t.then(() => new Promise(t => {
        t.Scroll = function () {
            $(document).ready(function () {
                !function (t, e = function () {
                }, n = []) {
                    t = jQuery(t), e = e.bind(t);
                    var o = -1, c = -1;
                    setInterval(function () {
                        t.height() == o && t.width() == c || (o = t.height(), c = t.width(), t.parent().animate({scrollTop: o}, 50), e.apply(null, n))
                    }, 100)
                }(".chat-content-container .chat-content-list", function (t) {
                }, [])
            })
        }, setTimeout(() => {
            let n = "rand_" + (new Date).getTime(), o = $(`<div id='${n}' class="box"></div>`);
            switch ($(".container2").append(o), e.type) {
                case"single_choice":
                    new Typed(`#${n}`, {
                        strings: [e.content], showCursor: !1, typeSpeed: 50, onComplete: () => {
                            let n = createSingleChoiceForm(e.answers);
                            n.find("[data-answer]").click(e => {
                                $(e.target).addClass("active"), n.find("[data-answer]:not(.active)").attr("disabled", !0), t()
                            }), o.append(n), e.afterMount && e.afterMount()
                        }
                    });
                    break;
                case"form":
                    let c = $(`template#${e.templateId}`).html();
                    o.append(c), e.afterMount && e.afterMount(), t();
                    break;
                case"text":
                    new Typed(`#${n}`, {
                        strings: [e.content], showCursor: !1, typeSpeed: 50, onComplete: () => {
                            e.afterMount && e.afterMount(), t(), t.Scroll()
                        }
                    })
            }
        }, 50)
    })), Promise.resolve());
    });
